from Crypto import Random
import string
from Crypto.Hash import SHA256
import sys
import os.path
import socket

class chap(object):
#inicializacao 
    def __init__(self, password):
        self.password = password
        self.ID = ""
        self.nonce = 0
#Geracao de um nonce e leitura/criacao do ID para serem enviados ao suplicante
    def auth_send(self):
        
        if  not os.path.exists("ID.txt"):
            fwrite = open("ID.txt","w")
            fwrite.write("0")
            fwrite.close()
        else:
            fread = open("ID.txt","r")
            self.ID = fread.read()
            fread.close() 		

        self.nonce = Random.new().read(16)
        return self.ID, self.nonce
#Verificacao da msg enviada pelo suplicante ao servidor. Devolve true se verificar, false em caso contrario	
    def auth_recv(self, msg):
        if not os.path.exists("ID.txt"):
            ID = 0
            fwrite = open("ID.txt","w")
            fwrite.write("0")
            fwrite.close()
        else:
            fread = open("ID.txt","r")
            ID = int(fread.read())
		
        msg_conf = str(ID) + self.nonce + self.password
        hash = SHA256.new()
        hash.update(msg_conf)
        msg_conf = hash.digest()
        self.ID = str(int(self.ID) + 1)
        fwrite = open("ID.txt","w")
        fwrite.write(str(self.ID))
        fwrite.close()
        if msg_conf == msg:
            return True
        else:
            return False		
#calcula a msg do lado do suplicante	
    def suppl(self, ID, nonce):

        msg = str(ID) + nonce + self.password
        hash = SHA256.new()
        hash.update(msg)
        msg = hash.digest()
        return msg

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "Wrong number of arguments."
        sys.exit()

    if sys.argv[1] == "--supplicant":
        c = chap(sys.argv[2])
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = 6666
        host = "127.0.0.1"
        s.connect((host , port))
        ID = s.recv(16)
        nonce = s.recv(16)
        msg = c.suppl(ID, nonce)
        s.sendall(msg)
        flag = s.recv(1)
        if flag == "1":
            print "Successful authentication"
        else:
            print "Unsuccessful authentication"
    elif sys.argv[1] == "--authenticator":
        c = chap(sys.argv[2])
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            s.bind(("127.0.0.1", 6666))
        except socket.error:
            print "Bind failed."
            sys.exit()
        s.listen(1)
        while 1:
            conn, addr = s.accept()
            print "Connection successful"
            id, nonce = c.auth_send()
            conn.sendall(str(id))
            conn.sendall(nonce)
            msg = conn.recv(32)
            flag = c.auth_recv(msg)
            if flag:
                print "Successful authentication"
                auth = "1"
            else:
                print "Unsuccessful authentication"
                auth = "0"
            conn.sendall(auth)