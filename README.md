# Challenge Handshake Authentication Protocol and Lamport Authentication Scheme

This repository contains Python implementations of both the Challenge Handshake Authentication Protocol (CHAP) and Lamport Authentication Scheme. It contains two programs, one for each protocol, that can act both as server and client. The program running in client mode will then attempt to authenticate with the server using the required protocol. The available source files are as follows:

* chap.py - containing the CHAP protocol server and client code.

* lamport.py - containing the Lamport scheme server and client code.

# User Manual

## CHAP
 
For CHAP, the program works in two different modes: supplicant and authenticator. For both modes, the secret must be passed as argument. It can, as such, be initialised as follows:

    chap.py --supplicant secret
 
    chap.py --authenticator secret

##Lamport

For Lamport Scheme, three different modes exist: supplicant initialisation, supplicant mode and authenticator. It can then be initialised as follows:

    lamport.py --initialize-supplicant secret

    lamport.py --supplicant

    lamport.py --authenticator secret

For both modes, *secret* is the secret that must be known by both supplicant and authenticator, but that is never sent from one to another during the authentication process.
# Author

Bernardo Sequeiros

For questions, please contact at bernardo.seq@gmail.com