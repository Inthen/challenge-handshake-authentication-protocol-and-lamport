from Crypto import Random
import random
from random import randrange
import string
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import sys
import os.path
import socket

BLOCK_SIZE = 16

key="0123456789abcdef"
#Classe de padding PKCS7
class padding(object): 

	def __init__(self,m) :
		self.m = m
	def pad(self) :
		npad = BLOCK_SIZE - (len(self.m) % BLOCK_SIZE)
		if npad == 0:
			npad = BLOCK_SIZE
		pad = chr(npad)
		return (self.m + pad * npad)
	def unpad(self) :
		pad = ord(self.m[-1])
		return self.m[:-pad]
#Classe de cifra AES-CBC, para cifrar os ficheiros h.txt
class AES_CBC(object):
    def __init__(self, key):
        self.key = key

    def cbcencrypt(self, m):
        p = padding(m)
        m = p.pad()
        iv = Random.new().read(BLOCK_SIZE)
        cipherf = AES.new(self.key[:BLOCK_SIZE], AES.MODE_CBC, iv)
        return (cipherf.encrypt(m) +iv) 

    def cbcdecrypt(self, c):
        iv = c[-BLOCK_SIZE:]
        cipherf = AES.new(self.key[:BLOCK_SIZE], AES.MODE_CBC, iv)
        c = c[:len(c)-BLOCK_SIZE]
        message = cipherf.decrypt(c)
        p = padding(message)
        message = p.unpad()
        return message	
#classe do esquema de Lamport
class Lamport(object):
#inicializacao
    def __init__(self, secret, i):
        self.secret = secret
        self.i = i
        self.cbc = AES_CBC(key)
#Usada quando se esgotam os i e gera um novo segredo	
    def set_secret(self, secret):
        self.secret = secret
#devolve o numero total de passwords one time geradas
    def get_max_i(self):
        return self.i
#criacao dos ficheiros hi.txt do suplicante
    def init_suppl(self):
        for j in range(0,self.i):
            fname = "h"+str(5-j)+".txt"
            fwrite = open(fname,"wb")
            hash = SHA256.new()
            hash.update(self.secret)
            self.secret = hash.digest()
            codedsecret = self.cbc.cbcencrypt(self.secret)
            fwrite.write(codedsecret)
            fwrite.close()
        self.secret = ""
#inicializacao do lado do autenticador
    def init_auth(self):
	    
        hash = SHA256.new()
        hash.update(self.secret)
        self.secret = hash.digest()
        self.curr_auth = 0
#devolve a password da atual identificacao
    def get_next_auth(self):
        aut = self.secret
        print self.curr_auth
        for j in range (0,(self.i-1) - self.curr_auth):
            hash = SHA256.new()
            hash.update(aut)
            aut = hash.digest()
        return aut
#utilizada pelo autenticador quando se esgotam o total de passwords. Gera um novo segredo, H(ht+ hi), onde i e gerado aleatoriamente
    def init_new_auth(self):
        new_i = random.randrange(1,5)
        new_secret = self.secret
        for i in range(0,(self.i-1) - new_i):
            hash = SHA256.new()
            hash.update(new_secret)
            new_secret = hash.digest() 
        hash = SHA256.new()
        hash.update(self.secret + new_secret)
        self.secret = hash.digest()
        self.curr_auth = 0
        return new_i
#devolve o i da password actual	
    def get_curr_auth(self):
        return self.curr_auth
#incrementa o i da autenticacao
    def inc_curr_auth(self):
        self.curr_auth = self.curr_auth + 1	
#le a password i do ficheiro respectivo
    def get_next_suppl(self, i):
        f = "h" + str(i+1) + ".txt"
        if not os.path.exists(f):
            print "Cannot find file. Initialize first with --initialize-supplicant mode"
            sys.exit()
        fread = open(f,"rb")
        fsecret = fread.read()
        fread.close()
        secret = self.cbc.cbcdecrypt(fsecret)
        return secret
	
if __name__ == "__main__":

    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print "Invalid number of arguments."
        sys.exit()

    if sys.argv[1] == "--initialize-supplicant" and len(sys.argv) == 3:
        l = Lamport(sys.argv[2],5)
        l.init_suppl()
        print "Supplicant initialized. Start with --supplicant to use supplicant mode"
        sys.exit()
    elif sys.argv[1] == "--authenticator" and len(sys.argv) == 3:
        l = Lamport(sys.argv[2],5)
        l.init_auth()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            s.bind(("127.0.0.1", 6666))
        except socket.error:
            print "Bind failed."
            sys.exit()
        s.listen(1)
        while 1:
            conn, addr = s.accept()
            print "Connection successful"
            i = l.get_curr_auth()
            if i >= l.get_max_i()-1:
                next_i = l.init_new_auth()
                conn.sendall(str(i))
                conn.sendall(str(next_i))
                i = l.get_curr_auth()
            conn.sendall(str(i))
            auth = l.get_next_auth()
            l.inc_curr_auth()
            msg = conn.recv(32)
            if auth == msg:
                conn.sendall("1")
                print "Authentication successful"
            else:
                conn.sendall("0")
                print "Authentication failed"
    elif sys.argv[1] == "--supplicant" and len(sys.argv) == 2:
        l = Lamport("",5)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = 6666
        host = "127.0.0.1"
        s.connect((host , port))
        i = int(s.recv(16))
        if i >= l.get_max_i()-1:
            next_i = int(s.recv(16))
            secret = l.get_next_suppl(next_i)
            ht = l.get_next_suppl(l.get_max_i()-1)
            new_secret = ht + secret
            l.set_secret(new_secret)
            l.init_suppl()
            i = int(s.recv(16))
            secret = l.get_next_suppl(i)
            s.sendall(secret)
            flag = s.recv(1)
            if flag == "1":
                print "Successful authentication"
            else:
                print "Unsuccessful authentication"
        else:
            secret = l.get_next_suppl(i)
            s.sendall(secret)
            flag = s.recv(1)
            if flag == "1":
                print "Successful authentication"
            else:
                print "Unsuccessful authentication" 